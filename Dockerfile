FROM ubuntu:focal
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    wait-for-it \
    && apt-get -y clean \
    && rm -rf /var/lib/apt/lists/*
ENTRYPOINT [ "wait-for-it" ]